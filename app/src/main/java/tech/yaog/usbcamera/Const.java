package tech.yaog.usbcamera;

public class Const {
    /**
     * UVC摄像头的VID，根据实际设备修改
     */
    public static final int VID = 0x0bda;
    /**
     * UVC摄像头的PID，根据实际设备修改
     */
    public static final int PID = 0x5842;
}
