package tech.yaog.usbcamera;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements TextureView.SurfaceTextureListener {

    private static final String TAG = MainActivity.class.getName();

    private TextureView surface;
    private ImageView image;

    private ServiceConnection serviceConnection;
    private CameraService cameraService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        surface = findViewById(R.id.surface);
        image = findViewById(R.id.image);

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                if (service instanceof CameraService.CameraBinder) {
                    CameraService.CameraBinder binder = (CameraService.CameraBinder) service;
                    cameraService = binder.getService();
                    if (drawSurface != null) {
                        cameraService.setPreviewDisplay(drawSurface);
                    }
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                cameraService = null;
            }
        };

        Intent intent = new Intent(this, CameraService.class);
        bindService(intent, serviceConnection, BIND_AUTO_CREATE);

        surface.setSurfaceTextureListener(this);

        surface.setOnClickListener(v -> {
            if (cameraService != null) {
                cameraService.doCapture(this::onCaptureResult);
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (cameraService != null) {
            cameraService.setPreviewDisplay((Surface) null);
        }
        unbindService(serviceConnection);
        super.onDestroy();
    }

    private void onCaptureResult(Bitmap bitmap) {
        image.post(() -> {
            image.setVisibility(View.VISIBLE);
            image.setImageBitmap(bitmap);
            image.postDelayed(() -> {
                image.setImageBitmap(null);
                image.setVisibility(View.GONE);
                bitmap.recycle();
            }, 2000);
        });

    }

    Surface drawSurface;

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.d(TAG, "onSurfaceTextureAvailable");

        drawSurface = new Surface(surface);
        if (cameraService != null) {
            cameraService.setPreviewDisplay(drawSurface);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.d(TAG, "onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.d(TAG, "onSurfaceTextureDestroyed");
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }
}
