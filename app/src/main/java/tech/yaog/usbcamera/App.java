package tech.yaog.usbcamera;

import android.app.Application;
import android.content.Intent;

import com.tencent.bugly.crashreport.CrashReport;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CrashReport.initCrashReport(getApplicationContext(), "e46c23ba10", false);

        Intent intent = new Intent(this, CameraService.class);
        intent.putExtra(CameraService.EXTRA_VID, Const.VID);
        intent.putExtra(CameraService.EXTRA_PID, Const.PID);
        intent.putExtra(CameraService.EXTRA_HEIGHT, 1080);
        intent.putExtra(CameraService.EXTRA_WIDTH, 1920);
        startService(intent);
    }
}
