package tech.yaog.hardware.usb;

public interface IButtonCallback {
    void onButton(int button, int state);
}
